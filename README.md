# PDO Database Bundle

## What it's for

Doctrine is great. Period. But for some use cases, for instance when you are trying to 
connect to a huge, preexisting database of some sorts, Doctrine generates a massive overhead.
In these situations, it may be handy to only use a PDO-connection of some sorts, and that's where 
this bundle comes into play.

### Key features

* Connect to a mysql, sqlite or sybase database through PDO
* Choose to use prepared statements or to execute queries directly
* Log your queries and their execution times into the symfony profiler

It doesn't have entities, it doesn't have repositories. This is only the connection.

## Installation

```bash
composer require fahrenholz/pdo-database-bundle
```

## Configuration

```yaml
sybase:
    driver:       "dblib"
    host:         "~"
    port:         2000
    dbname:       "~"
    user:         "~"
    password:     "~"
    charset:      UTF8
mysql:
    driver:       "mysql"
    host:         "~"
    port:         3306
    dbname:       "~"
    user:         "~"
    password:     "~"
    charset:      UTF8
sqlite:
    driver:       "sqlite"
    path:         "/path/to/my/db.sqlite"
    user:         ""
    password:     ""

```

## Usage

```php

$connection = $this->get('fahrenholz_pdo_database.connections')->get('mysql');
$mySQL = "SELECT * FROM my_table WHERE parameter1 = :parameter1 AND parameter2 = :parameter2";
//build and execute prepared statement
$result = $connection->execute(
    $mySQL,
    [
        ":parameter1" => $myParameter,
        ":parameter2" => $myParameterTwo,
    ]
);

//execute query without prepared statements
$result = $connection->executeSimple(
    $mySQL,
    [
        ":parameter1" => $myParameter,
        ":parameter2" => $myParameterTwo,
    ]
);


//get all results as objects
$allRows = $result->getAll(Collection::GET_OBJ);
//get all results as sequential arrays
$allRows = $result->getAll(Collection::GET_ARRAY);
//get all results as associative arrays
$allRows = $result->getAll(Collection::GET_ASSOC);

//reset counter
$result->resetCounter();

//execute something on each result
for ($i=0; $i<$result->getRowCount();$i++) {
    $res = $result->get(Collection::GET_OBJ);
    //do something
}

//get the SQL generated for a query
$connection->getSQL(
    $mySQL, 
    [
        ":parameter1" => $myParameter,
        ":parameter2" => $myParameterTwo,
    ]
);

```

## Tests

MySQL-Connectivity and SQLite-Connectivity as well as all generic classes are well
covered with unit tests.

```bash
composer install
./vendor/bin/codecept run
```

The sybase connectivity is well tested, but against a proprietary Database which can't be 
exposed via a public gitlab account. You'll have to trust me on that or create some tests of your own
Every new connection has to be tested via a connection-test-class implementing the ConnectionTestInterface
for consistency.

## Contribution

Please feel free to contribute. Still Todo are following items:

* pgsql-connectivity
* mssql-connectivity
* oracle-connectivity
