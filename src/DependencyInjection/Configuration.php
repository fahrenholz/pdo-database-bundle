<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 *
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * Configuration-class of the bundle Bundles
 *
 * @category  DependencyInjection
 * @package   Fahrenholz\PdoDatabaseBundle\DependencyInjection
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('fahrenholz_pdo_database');
        $rootNode
            ->useAttributeAsKey("name")
            ->prototype('array')
                ->children()
                    ->scalarNode('driver')->isRequired()->end()
                    ->scalarNode('host')->end()
                    ->integerNode('port')->end()
                    ->scalarNode('dbname')->end()
                    ->scalarNode('path')->end()
                    ->scalarNode('user')->isRequired()->end()
                    ->scalarNode('password')->isRequired()->end()
                    ->booleanNode('persistent_connection')
                        ->defaultFalse()
                    ->end()
                    ->scalarNode('charset')->end()
                ->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
