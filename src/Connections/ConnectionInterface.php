<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\Connections;

use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidParameterException;
use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidSqlParameterException;
use Fahrenholz\PdoDatabaseBundle\Result\Collection;
use Fahrenholz\PdoDatabaseBundle\Exceptions\NoResultException;

/**
 * Interface ConnectionInterface
 *
 * Interface for Connection-objects
 *
 * @category  Connection
 * @package   Fahrenholz\PdoDatabaseBundle\Connections
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
interface ConnectionInterface
{
    /**
     * execute
     *
     * Executes an SQL-Query with prepared statements
     *
     * @param string $sql        SQL-Query
     * @param array  $parameters Array of parameters
     *
     * @return Collection
     * @throws NoResultException
     * @throws InvalidSqlParameterException
     * @throws InvalidParameterException
     */
    public function execute(string $sql, array $parameters = []);

    /**
     * executeSimple
     *
     * Executes a query without SQL-Statements
     *
     * @param string $sql        SQL-Query
     * @param array  $parameters Parameter-Array
     *
     * @return Collection
     * @throws NoResultException
     * @throws InvalidSqlParameterException
     * @throws InvalidParameterException
     */
    public function executeSimple(string $sql, array $parameters = []);

    /**
     * getSql
     *
     * Gives back the formatted SQL without executing it
     *
     * @param string $sql        SQL-Query
     * @param array  $parameters Parameter-Array
     *
     * @return mixed
     * @throws InvalidSqlParameterException
     */
    public function getSql(string $sql, array $parameters = []);
}
