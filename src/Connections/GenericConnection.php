<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 *
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\Connections;

use PDO;
use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidParameterException;
use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidSqlParameterException;
use Fahrenholz\PdoDatabaseBundle\Exceptions\NoResultException;
use Fahrenholz\PdoDatabaseBundle\Result\Collection;

/**
 * Class GenericConnection
 *
 * Generic connection class (fit for mysql and for factorization of some methods)
 *
 * @category  Connection
 * @package   Fahrenholz\PdoDatabaseBundle\Connections
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class GenericConnection implements ConnectionInterface
{
    /**
     * @type string
     */
    const DRIVER_KEY = "driver";

    /**
     * @type string
     */
    const DBNAME_KEY = "dbname";

    /**
     * @type string
     */
    const PWD_KEY = "password";

    /**
     * @type string
     */
    const HOST_KEY = "host";

    /**
     * @type string
     */
    const PORT_KEY = "port";

    /**
     * @type string
     */
    const USER_KEY = "user";

    /**
     * @type string
     */
    const CONNTYPE_KEY = "persistent_connection";

    /**
     * @var PDO
     */
    protected $connection;

    /**
     * @var array
     */
    protected $executedQueries = [];

    /**
     * @var bool
     */
    protected $supportsMultipleRowsets = true;

    /**
     * GenericConnection constructor.
     *
     * @param array $configuration Konfiguration der Connection
     *
     * @throws InvalidParameterException
     */
    public function __construct(array $configuration)
    {
        if (!$this->checkOptions($configuration)) {
            throw new InvalidParameterException("Database configuration is missing some required parameters");
        }

        $dsn = "{$configuration[self::DRIVER_KEY]}:host={$configuration[self::HOST_KEY]};port={$configuration[self::PORT_KEY]};dbname={$configuration[self::DBNAME_KEY]}";

        $this->buildConnection($dsn, $configuration);
    }

    /**
     * execute
     *
     * Executes an SQL-Query with prepared statements
     *
     * @param string $sql        SQL-Query
     * @param array  $parameters Array of parameters
     *
     * @return Collection
     * @throws NoResultException
     * @throws InvalidSqlParameterException
     * @throws InvalidParameterException
     */
    public function execute(string $sql, array $parameters = [])
    {
        $startTime = microtime(true);
        $queryLogObject = (object) [
            "function" => "execute",
            "sql" => $this->getSql($sql, $parameters),
            "executionTime" => null,
        ];
        $statement = $this->connection->prepare(
            $sql,
            [
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ]
        );

        /*
         * Sybase does not clearly map int and string types when using PDO. We have to do it by hand.
         */
        foreach ($parameters as $key => $value) {
            $variableName = substr($key, 1);
            if (gettype($value) == "double") {
                //PARAM_FLOAT existiert nicht, wir müssen den Wert als String übergeben
                $value = (string) $value;
            }
            ${$variableName} = $value;
            $type = $this->getPDOType($value);
            $statement->bindParam($key, ${$variableName}, $type);
        }

        $statement->execute();
        $collection = new Collection();
        $collection->set($statement->fetchAll(PDO::FETCH_ASSOC));

        $statement->closeCursor();

        if ($collection->getRowCount() == 0) {
            throw new NoResultException("Database delivered no result");
        }

        $queryLogObject->executionTime = microtime(true) - $startTime;
        $this->executedQueries[(string) $startTime] = $queryLogObject;

        return $collection;
    }

    /**
     * executeSimple
     *
     * Executes a query without SQL-Statements
     *
     * @param string $sql        SQL-Query
     * @param array  $parameters Parameter-Array
     *
     * @return Collection
     * @throws NoResultException
     * @throws InvalidSqlParameterException
     * @throws InvalidParameterException
     */
    public function executeSimple(string $sql, array $parameters = [])
    {
        $startTime = microtime(true);
        $queryLogObject = (object) [
            "function" => "executeStoredProcedure",
            "sql" => $this->getSql($sql, $parameters),
            "executionTime" => null,
        ];

        foreach ($parameters as $key => $param) {
            if (gettype($param) == "string") {
                $param = $this->connection->quote((string) $param, $this->getPDOType($param));
            }
            $sql = str_replace($key, $param, $sql);
        }

        $statement = $this->connection->query($sql);
        $collection = new Collection();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        if ($result !== false) {
            $collection->set($result);
        }

        if ($this->supportsMultipleRowsets) {
            do {
                $result = $statement->fetchAll(PDO::FETCH_ASSOC);
                if ($result !== false) {
                    $collection->set($result);
                }
            } while ($statement->nextRowset() !== false);
        }

        $statement->closeCursor();

        if ($collection->getRowCount() == 0) {
            throw new NoResultException("Database delivered no result");
        }

        $queryLogObject->executionTime = microtime(true) - $startTime;
        $this->executedQueries[(string) $startTime] = $queryLogObject;

        return $collection;
    }

    /**
     * getSql
     *
     * Gives back the formatted SQL without executing it
     *
     * @param string $sql        SQL-Query
     * @param array  $parameters Parameter-Array
     *
     * @return mixed
     * @throws InvalidSqlParameterException
     */
    public function getSql(string $sql, array $parameters = [])
    {
        foreach ($parameters as $key => $param) {
            if (gettype($param) == "string") {
                $param = $this->connection->quote((string) $param, $this->getPDOType($param));
            }
            $sql = str_replace($key, $param, $sql);
        }

        return $sql;
    }

    /**
     * getQueryLog
     *
     * Gives back the query log
     *
     * @return array
     */
    public function getQueryLog()
    {
        return $this->executedQueries;
    }

    /**
     * getPDOType
     *
     * Determines the accurate PDO-type
     *
     * @param mixed $value Wert
     *
     * @return int
     * @throws InvalidSqlParameterException
     */
    protected function getPDOType($value)
    {
        $type = gettype($value);
        $invalidTypes = ["unknown type", "array", "object", "resource"];

        $pdoType = PDO::PARAM_STR;
        if (in_array($type, $invalidTypes)) {
            throw new InvalidSqlParameterException("PDO can't treat elements of type '{$type}' as arguments");
        } elseif ($type == "boolean") {
            $pdoType = PDO::PARAM_BOOL;
        } elseif ($type == "integer") {
            $pdoType = PDO::PARAM_INT;
        } elseif ($type = "NULL") {
            $pdoType = PDO::PARAM_NULL;
        }

        return $pdoType;
    }

    /**
     * checkOptions
     *
     * Checks the given configuration options
     *
     * @param array $options
     *
     * @return bool
     */
    protected function checkOptions(array $options)
    {
        return (
            $this->checkDriver($options)
            && $this->checkHost($options)
            && $this->checkDbName($options)
            && $this->checkCredentials($options)
        );
    }

    /**
     * checkDriver
     *
     * Checks the existence of a driver in the configuration options
     *
     * @param array $options Optionen
     *
     * @return bool
     */
    protected function checkDriver(array $options)
    {
        return (isset($options[self::DRIVER_KEY]) && is_string(self::DRIVER_KEY));
    }

    /**
     * checkHost
     *
     * Checks the existence of a host in the configuration options
     *
     * @param array $options Optionen
     *
     * @return bool
     */
    protected function checkHost(array $options)
    {
        return (
            isset($options[self::HOST_KEY])
            && is_string($options[self::HOST_KEY])
            && isset($options[self::PORT_KEY])
            && is_int($options[self::PORT_KEY])
        );
    }

    /**
     * checkDbName
     *
     * checks the existence of a database-name in the configuration options
     *
     * @param array $options Optionen
     *
     * @return bool
     */
    protected function checkDbName(array $options)
    {
        return (isset($options[self::DBNAME_KEY]) && is_string($options[self::DBNAME_KEY]));
    }

    /**
     * checkCredentials
     *
     * Checks the existence of credentials in the configuration options
     *
     * @param array $options Optionen
     *
     * @return bool
     */
    protected function checkCredentials(array $options)
    {
        return (
            isset($options[self::USER_KEY])
            && is_string($options[self::USER_KEY])
            && $this->checkPassword($options)
        );
    }

    /**
     * checkPassword
     *
     * Checks the existence of a password in the configuration options
     *
     * @param array $options Optionen
     *
     * @return bool
     */
    protected function checkPassword(array $options)
    {
        return (isset($options[self::PWD_KEY]) && is_string($options[self::PWD_KEY]) && trim($options[self::PWD_KEY]) != "~");
    }

    /**
     * buildConnection
     *
     * Creates a PDO connection using the given DSN and configuration
     *
     * @param string $dsn
     * @param array  $configuration
     *
     */
    protected function buildConnection(string $dsn, array $configuration)
    {
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => false,
        ];

        if (isset($configuration[self::CONNTYPE_KEY]) && $configuration[self::CONNTYPE_KEY] === true) {
            $options[PDO::ATTR_PERSISTENT] = true;
        }

        try {
            $this->connection = new PDO($dsn, $configuration[self::USER_KEY], $configuration[self::PWD_KEY], $options);
        } catch (\PDOException $e) {
            if ($e->getCode() != 20006) {
                throw $e;
            }
            $this->connection = null;
            $this->connection = new PDO($dsn, $configuration[self::USER_KEY], $configuration[self::PWD_KEY], $options);
        }
    }


}
