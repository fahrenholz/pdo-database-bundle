<?php
/**
 * Diese Datei ist Bestandteil des Strato Name Bundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@strato.de>
 * © Strato AG
 *
 * Date: 02.01.18
 * Time: 12:15
 *
 * KurzInfo zur Datei
 */

namespace Fahrenholz\PdoDatabaseBundle\Connections;
use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidParameterException;

/**
 * Class SqliteConnection
 *
 * Description
 *
 * @category
 * @package   Fahrenholz\PdoDatabaseBundle\Connections
 * @author    Vincent Fahrenholz <fahrenholz@strato.de>
 * @copyright 2017 Strato AG
 * @version   Release: 1.0.0
 */
class SqliteConnection extends GenericConnection implements ConnectionInterface
{
    /**
     * @type string
     */
    const PATH_KEY = "path";

    protected $supportsMultipleRowsets = false;

    /**
     * SqliteConnection constructor.
     *
     * @param array $configuration
     *
     * @throws InvalidParameterException
     */
    public function __construct(array $configuration)
    {
        if (!$this->checkOptions($configuration)) {
            throw new InvalidParameterException("Database configuration is missing some required parameters");
        }

        $dsn = "{$configuration[self::DRIVER_KEY]}:{$configuration[self::PATH_KEY]}";

        $this->buildConnection($dsn, $configuration);
    }

    /**
     * checkOptions
     *
     * checks the configured options
     *
     * @param array $options
     *
     * @return bool
     */
    protected function checkOptions(array $options)
    {
        return (
            $this->checkDriver($options)
            && $this->checkPath($options)
        );
    }

    /**
     * checkPath
     *
     * checks if there is a path given and if this path is correct.
     *
     * @param array $options
     *
     * @return bool
     */
    public function checkPath(array $options)
    {
        return (
            isset($options[self::PATH_KEY])
            && is_dir(dirname($options[self::PATH_KEY]))
            && (
                (is_file($options[self::PATH_KEY]) && is_writable($options[self::PATH_KEY]))
                || (!is_file($options[self::PATH_KEY]) && is_writable(dirname($options[self::PATH_KEY])))
            )
        );
    }
}
