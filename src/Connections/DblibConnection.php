<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 *
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\Connections;


use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidParameterException;

/**
 * Class DblibConnection
 *
 * Sybase-specific Connection
 *
 * @category  Connection
 * @package   Fahrenholz\PdoDatabaseBundle\Connections
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class DblibConnection extends GenericConnection implements ConnectionInterface
{
    /**
     * DblibConnection constructor.
     *
     * @param array $configuration
     *
     * @throws InvalidParameterException
     */
    public function __construct(array $configuration)
    {
        if (!$this->checkOptions($configuration)) {
            throw new InvalidParameterException("Database configuration is missing some required parameters");
        }

        $dsn = "{$configuration[self::DRIVER_KEY]}:host={$configuration["host"]}:{$configuration["port"]};dbname={$configuration["dbname"]}";

        $this->buildConnection($dsn, $configuration);
    }
}
