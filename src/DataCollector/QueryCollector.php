<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 *
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\DataCollector;


use Fahrenholz\PdoDatabaseBundle\Services\ConnectionsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

/**
 * Class QueryCollector
 *
 * Collector for all queries and metrics about these queries
 *
 * @category  DataCollector
 * @package   Fahrenholz\PdoDatabaseBundle\DataCollector
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class QueryCollector extends DataCollector
{

    /**
     * @var ConnectionsService
     */
    private $connectionsService;

    private $vars;

    /**
     * QueryCollector constructor.
     *
     * @param ConnectionsService $connectionsService
     */
    public function __construct(ConnectionsService $connectionsService)
    {
        $this->connectionsService = $connectionsService;
    }

    /**
     * collect
     *
     * Bereitet die Daten für den Profiler auf
     *
     * @param Request         $request
     * @param Response        $response
     * @param \Exception|null $exception
     *
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->vars = [
            "request" => $request,
            "response" => $response,
            "exception" => $exception,
        ];
        $this->data = $this->connectionsService->getQueryLogs();
    }

    /**
     * reset
     *
     * Resets the query collector
     */
    public function reset()
    {
        $this->vars = [];
        $this->data = [];
    }

    /**
     * getQueries
     *
     * gibt alle queries zurück
     *
     * @return array
     */
    public function getQueries()
    {
        return $this->data;
    }

    /**
     * getTotalExecutionTime
     *
     * Gibt die summierte Execution time aus
     *
     * @return float
     */
    public function getTotalExecutionTime()
    {
        $sum = 0.0;
        foreach ($this->data as $query) {
            $sum += $query->executionTime;
        }

        return $sum;
    }

    /**
     * getName
     *
     * Gibt den Namen zurück
     *
     * @return string
     */
    public function getName()
    {
        return "fahrenholz_pdo_database.query_collector";
    }
}
