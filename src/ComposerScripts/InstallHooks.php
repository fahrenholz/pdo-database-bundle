<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\ComposerScripts;

use Composer\Script\Event;

/**
 * Class InstallHooks
 *
 * ComposerScriptClass for the Install of the Pre-Commit-Hook
 *
 * @category  ComposerScript
 * @package   Fahrenholz\PdoDatabaseBundle\ComposerScripts
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class InstallHooks
{
    /**
     * installPreCommitHook
     *
     * Installs the pre-commit-hook
     *
     * @param Event $event
     *
     */
    public static function installPreCommitHook(Event $event)
    {
        $rootPath = dirname($event->getComposer()->getConfig()->get('vendor-dir'))."/";
        $hookPath = $rootPath.".git/hooks/pre-commit";
        $binaryPath = $rootPath."bin/pre-commit";
        self::createLink($hookPath, $binaryPath);
    }

    /**
     * createLink
     *
     * creates the hook-symlink
     *
     * @param string $hookPath   Pfad im Hook-ordner (Link)
     * @param string $binaryPath Pfad der binary (Target)
     *
     */
    private static function createLink(string $hookPath, string $binaryPath)
    {
        $msg = "Hook was already installed - Nothing changed\n";
        if (!is_link($hookPath)) {
            if (file_exists($hookPath)) {
                unlink($hookPath);
            }
            symlink($binaryPath, $hookPath);
            echo "Hook has been installed\n";
        }

        echo $msg;
    }
}
