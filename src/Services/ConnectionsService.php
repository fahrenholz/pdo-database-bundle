<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 *
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\Services;


use Fahrenholz\PdoDatabaseBundle\Connections\DblibConnection;
use Fahrenholz\PdoDatabaseBundle\Connections\GenericConnection;
use Fahrenholz\PdoDatabaseBundle\Connections\SqliteConnection;
use Fahrenholz\PdoDatabaseBundle\Exceptions\ConnectionNotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConnectionsService
 *
 * Service-class for accessing the connections
 *
 * @category  Service
 * @package   Fahrenholz\PdoDatabaseBundle\Services
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class ConnectionsService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $connections = [];

    /**
     * ConnectionsService constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->registerConnections();
    }

    /**
     * registerConnections
     *
     * Registers a connection and opens it
     */
    private function registerConnections()
    {
        $config = $this->container->getParameter("fahrenholz_pdo_database.config");
        foreach ($config as $key => $connection) {
            switch ($connection["driver"]) {
                case "dblib":
                    $this->connections[$key] = new DblibConnection($connection);
                    break;
                case "sqlite":
                    $this->connections[$key] = new SqliteConnection($connection);
                    break;
                case "mysql":
                default:
                    $this->connections[$key] = new GenericConnection($connection);
                    break;
            }
        }
    }

    /**
     * get
     *
     * Gives back the demanded connection
     *
     * @param string $connectionName name of the demanded connection
     *
     * @return mixed
     * @throws ConnectionNotFoundException
     */
    public function get(string $connectionName)
    {
        if (!isset($this->connections[$connectionName])) {
            throw new ConnectionNotFoundException("Connection {$connectionName} does not exist");
        }

        return $this->connections[$connectionName];
    }

    /**
     * getQueryLogs
     *
     * Aggregates the query logs of all configured connections
     *
     * @return array
     */
    public function getQueryLogs()
    {
        $logs = [];

        foreach ($this->connections as $connection) {
            $logs = array_merge($logs, $connection->getQueryLog());
        }

        ksort($logs);

        return $logs;
    }

}
