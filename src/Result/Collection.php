<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 *
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\Result;


use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidParameterException;

/**
 * Class Collection
 *
 *Data-class for result objects
 *
 * @category  Result
 * @package   Fahrenholz\PdoDatabaseBundle\Result
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class Collection
{
    /**
     * @var array Items der Collection
     */
    protected $items = [];

    /**
     * @var int Next position to be fetched in array
     */
    protected $cursor = 0;

    /**
     * @type int Get-Style-Konstant - Get as associative array or list of associative arrays
     */
    const GET_ASSOC = 1;

    /**
     * @type int Get-Style-Konstant - Get as object or list of objekts
     */
    const GET_OBJ = 2;

    /**
     * @type int Get-Style-Konstant - Get as sequential array or list of sequential arrays
     */
    const GET_ARRAY = 3;

    /**
     * append
     *
     * Appends an item to the collection
     *
     * @param array $item
     * @throws InvalidParameterException
     */
    public function append(array $item)
    {
        if (!$this->isAssoc($item)) {
            throw new InvalidParameterException("expects association, sequential array given");
        }
        $this->items[] = $item;
    }

    /**
     * set
     *
     * appends an associative array of items to the collection
     *
     * @param array $items
     * @throws InvalidParameterException
     */
    public function set(array $items)
    {
        foreach ($items as $item) {
            if (!$this->isAssoc($item)) {
                $type = gettype($item);
                throw new InvalidParameterException("Parameter Items contains {$type}, must be assocs");
            }
            $this->append($item);
        }
    }

    /**
     * push
     *
     * Pushes a new item to the beginning of the collection
     *
     * @param array $item
     * @throws InvalidParameterException
     */
    public function push(array $item)
    {
        if (!$this->isAssoc($item)) {
            throw new InvalidParameterException("expects association, sequential array given");
        }
        $this->items = array_merge([$item], $this->items);
    }

    /**
     * get
     *
     * Gets the item under the current cursor position
     *
     * @param int $getStyle Style of the item (Array, Assoc, Objekt)
     *
     * @return array|bool|object
     */
    public function get(int $getStyle = self::GET_OBJ)
    {
        $result = false;

        if ($this->cursor != count($this->items)) {
            switch ($getStyle) {
                case self::GET_ASSOC:
                    $result = $this->items[$this->cursor];
                    break;
                case self::GET_ARRAY:
                    $result = array_values($this->items[$this->cursor]);
                    break;
                case self::GET_OBJ:
                default:
                    $result = (object) $this->items[$this->cursor];
                    break;
            }

            $this->cursor++;
        }

        return $result;
    }

    /**
     * getAll
     *
     * Gets all items of the collection
     *
     * @param int $getStyle Style des Items (Array, Assoc, Objekt)
     *
     * @return array
     */
    public function getAll(int $getStyle = self::GET_OBJ)
    {
        $result = [];
        $this->resetCursor();
        while (($item = $this->get($getStyle)) !== false) {
            $result[] = $item;
        }

        return $result;
    }

    /**
     * getRowCount
     *
     * Gets the total number of items of the collection
     *
     * @return int
     */
    public function getRowCount()
    {
        return count($this->items);
    }

    /**
     * resetCursor
     *
     * Sets the cursor on the first item of the collection
     */
    public function resetCursor()
    {
        $this->cursor = 0;
    }

    /**
     * isAssoc
     *
     * Determines if an item is an associative array
     *
     * @param mixed $item
     *
     * @return bool
     */
    private function isAssoc($item)
    {
        return (
            is_array($item) && (
                array_keys($item) !== range(0, count($item) - 1)
                || count($item) == 0
            )
        );
    }
}
