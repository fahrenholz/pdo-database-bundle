<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 *
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */

namespace Fahrenholz\PdoDatabaseBundle\Exceptions;

use Exception;

/**
 * Class InvalidParameterException
 *
 * EException thrown when a parameter is not quite as requested
 *
 * @category  Exception
 * @package   Fahrenholz\PdoDatabaseBundle\Exceptions
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class InvalidParameterException extends Exception
{

}
