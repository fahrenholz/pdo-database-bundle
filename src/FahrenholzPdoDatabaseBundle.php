<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 *
 * @license MIT
 * @copyright 2017 Vincent Fahrenholz
 */


namespace Fahrenholz\PdoDatabaseBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class FahrenholzPdoDatabaseBundle
 *
 * Main bundle-class
 *
 * @category  Bundle
 * @package   Fahrenholz\PdoDatabaseBundle
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class FahrenholzPdoDatabaseBundle extends Bundle
{
}
