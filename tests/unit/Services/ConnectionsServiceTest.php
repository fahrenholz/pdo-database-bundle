<?php


namespace Fahrenholz\PdoDatabaseBundle\Tests\Generic\Services;


use Fahrenholz\PdoDatabaseBundle\Connections\GenericConnection;
use Fahrenholz\PdoDatabaseBundle\Exceptions\ConnectionNotFoundException;
use Fahrenholz\PdoDatabaseBundle\Services\ConnectionsService;

/**
 * Class ConnectionsServiceTest
 *
 * Tests the connectionsservice class
 *
 * @category  Test
 * @package   Fahrenholz\PdoDatabaseBundle\Tests\Generic\Services
 * @author    Vincent Fahrenholz <fahrenholz@strato.de>
 * @copyright 2017 Strato AG
 * @version   Release: 1.0.0
 */
class ConnectionsServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ConnectionsService $service
     */
    protected $service;

    /**
     * _before
     *
     * Bootstraps the service
     */
    protected function _before()
    {
        $container = require __DIR__ . "/../../_support/_bootstrap.php";
        $this->service = new ConnectionsService($container);
    }

    /**
     * testGetMissingDatabase
     *
     * Tests if the service throws an exception when requesting an inexistent connection
     */
    public function testGetMissingDatabase()
    {
        $this->expectException(ConnectionNotFoundException::class);
        $this->service->get("blablubb");
    }

    /**
     * testGetExistingDatabase
     *
     * Tests if the service correctly gives back an existing connection
     */
    public function testGetExistingDatabase()
    {
        $connection = $this->service->get("mysql");
        $this->assertInstanceOf(GenericConnection::class, $connection);
    }

    /**
     * testReturnQueryLog
     *
     * Tests the query log
     */
    public function testReturnQueryLog()
    {
        $mysqlConnection = $this->service->get("mysql");
        $sqlMysql = "SELECT count(*) AS rowsCount FROM test_data";
        $sqlMysql2 = "SELECT * FROM test_data";

        $mysqlConnection->execute($sqlMysql);
        $mysqlConnection->execute($sqlMysql2);

        $queries = $this->service->getQueryLogs();


        $keys = array_keys($queries);

        $this->assertInternalType("array", $queries);
        $this->assertCount(2, $queries);
        $this->assertEquals($sqlMysql, $queries[$keys[0]]->sql);
        $this->assertEquals($sqlMysql2, $queries[$keys[1]]->sql);
    }

}
