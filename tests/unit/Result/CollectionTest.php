<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 * @license MIT
 *
 * Date: 06.09.17
 * Time: 14:29
 *
 * KurzInfo zur Datei
 */

namespace Fahrenholz\PdoDatabaseBundle\Tests\Generic\Result;


use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidParameterException;
use Fahrenholz\PdoDatabaseBundle\Result\Collection;

/**
 * Class CollectionTest
 *
 * Tests for the collection class
 *
 * @category  Test
 * @package   Fahrenholz\PdoDatabaseBundle\Tests\Generic\Result
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class CollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * testCreationFailureWithSequentialArrays
     *
     * Tests that set() doesn't work with an array of sequential arrays
     */
    public function testCreationFailureWithSequentialArrays()
    {
        $this->expectException(InvalidParameterException::class);
        $items = [
            [
                "test1"
            ],
            [
                "test2"
            ],
        ];
        $collection = new Collection();
        $collection->set($items);
    }

    /**
     * testCreationFailureWithScalars
     *
     * Tests that set() doesn't work with an array of scalars
     */
    public function testCreationFailureWithScalars()
    {
        $this->expectException(InvalidParameterException::class);
        $items = [
            "test1",
            "test2"
        ];
        $collection = new Collection();
        $collection->set($items);
    }

    /**
     * testCreationSuccess
     *
     * Tests that set() works with an array of associative arrays
     */
    public function testCreationSuccess()
    {
        $collection = $this->getCollection();
        $this->assertInstanceOf(Collection::class, $collection);
    }

    /**
     * testAppendFailure
     *
     * Tests that append() doesn't work  without an associative array as parameter
     */
    public function testAppendFailure()
    {
        $this->expectException(InvalidParameterException::class);
        $collection = $this->getCollection();
        $collection->append([
            "dings"
        ]);
    }

    /**
     * testAppend
     *
     * Tests that append() appends an item to the collection
     */
    public function testAppend()
    {
        $collection = $this->getCollection();
        $collection->append([
            "key" => "value3"
        ]);

        $items = $collection->getAll();
        $this->assertEquals(3, count($items), "The item-count is not as expected");
        $this->assertEquals("value3", $items[2]->key, "The last item hasn't the expected value");
    }

    /**
     * testPushFailure
     *
     * Tests that push() doesn't work without an associative array as parameter
     */
    public function testPushFailure()
    {
        $this->expectException(InvalidParameterException::class);
        $collection = $this->getCollection();
        $collection->push([
            "dings"
        ]);
    }

    /**
     * testPush
     *
     * Tests that push() pushes an item to the beginning of a collection
     */
    public function testPush()
    {
        $collection = $this->getCollection();
        $collection->push([
            "key" => "value3"
        ]);

        $items = $collection->getAll();
        $this->assertEquals(3, count($items), "The item-count is not as expected");
        $this->assertEquals("value3", $items[0]->key, "The first item hasn't the expected value");
    }

    /**
     * testGetAsDefault
     *
     * Tests that get() called without parameters gives back an object
     */
    public function testGetAsDefault()
    {
        $collection = $this->getCollection();
        $item = $collection->get();
        $this->checkIfObject($item);
    }

    /**
     * testGetAsObject
     *
     * Tests that get() called with Collection::GET_OBJ gives back an object
     */
    public function testGetAsObject()
    {
        $collection = $this->getCollection();
        $item = $collection->get(Collection::GET_OBJ);
        $this->assertInternalType("object", $item, "Item isn't an object");
    }

    /**
     * testGetAsArray
     *
     * Tests that get() called with Collection::GET_ARRAY gives back a sequential array
     */
    public function testGetAsArray()
    {
        $collection = $this->getCollection();
        $item = $collection->get(Collection::GET_ARRAY);
        $this->checkIfArray($item);
    }

    /**
     * testGetAsAssoc
     *
     * Tests that get() called with Collection::GET_ASSOC gives back an associative array
     */
    public function testGetAsAssoc()
    {
        $collection = $this->getCollection();
        $item = $collection->get(Collection::GET_ASSOC);
        $this->checkIfAssoc($item);
    }

    /**
     * testGetValue
     *
     * Tests that get() gives back the correct value
     */
    public function testGetValue()
    {
        $collection = $this->getCollection();
        $item = $collection->get();
        $this->assertEquals($item->key, "value1", "item->key hasn't the expected value");
    }

    /**
     * testGetMultiple
     *
     * Tests if the cursor position changes after a get()
     */
    public function testGetMultiple()
    {
        $collection = $this->getCollection();
        $item = $collection->get();
        $item2 = $collection->get();
        $this->assertEquals("value1", $item->key, "The first item hasn't the expected value");
        $this->assertEquals("value2", $item2->key, "The second item hasn't the expected value");
    }

    /**
     * testResetCursor
     *
     * Tests if resetCursor() is working
     */
    public function testResetCursor()
    {
        $collection = $this->getCollection();
        $item = $collection->get();
        $collection->resetCursor();
        $item2 = $collection->get();
        $this->assertEquals($item->key, $item2->key, "The elements are not identical");
    }

    /**
     * testGetAllDefault
     *
     * Tests if getAll() without parameters gives back an object
     */
    public function testGetAllDefault()
    {
        $collection = $this->getCollection();
        $items = $collection->getAll();
        foreach ($items as $item) {
            $this->checkIfObject($item);
        }
    }

    /**
     * testGetAllAsObject
     *
     * Tests if getAll() with the parameter GET_OBJ gives back an object
     */
    public function testGetAllAsObject()
    {
        $collection = $this->getCollection();
        $items = $collection->getAll(Collection::GET_OBJ);
        foreach ($items as $item) {
            $this->checkIfObject($item);
        }
    }

    /**
     * testGetAllAsArray
     *
     * Tests if getAll() with the parameter GET_ARRAY gives back a sequential array
     */
    public function testGetAllAsArray()
    {
        $collection = $this->getCollection();
        $items = $collection->getAll(Collection::GET_ARRAY);
        foreach ($items as $item) {
            $this->checkIfArray($item);
        }
    }

    /**
     * testGetAllAsAssoc
     *
     * Tests if getAll() with the parameter GET_ASSOC gives back an associative array
     */
    public function testGetAllAsAssoc()
    {
        $collection = $this->getCollection();
        $items = $collection->getAll(Collection::GET_ASSOC);
        foreach ($items as $item) {
            $this->checkIfAssoc($item);
        }
    }

    /**
     * testGetRowCount
     *
     * Tests if getRowCount() works
     */
    public function testGetRowCount()
    {
        $collection = $this->getCollection();
        $this->assertEquals(2, $collection->getRowCount(), "getRowCount doesn't give back the expected result");
    }

    /**
     * getCollection
     *
     * Creates a dummy collection object
     *
     * @return Collection
     * @throws InvalidParameterException
     */
    private function getCollection()
    {
        $items = [
            [
                "key"=>"value1"
            ],
            [
                "key"=>"value2"
            ],
        ];

        $collection = new Collection();
        $collection->set($items);

        return $collection;
    }

    /**
     * checkIfObject
     *
     * Checks if item is an object
     *
     * @param $item
     */
    private function checkIfObject($item)
    {
        $this->assertInternalType("object", $item, "Item isn't an object");
    }

    /**
     * checkIfArray
     *
     * Checks if item is a sequential array
     *
     * @param $item
     */
    private function checkIfArray($item)
    {
        $this->assertInternalType("array", $item, "Item isn't an array");
        $this->assertEquals(true, isset($item[0]), "Item isn't a sequential array");
        $this->assertEquals(false, isset($item["key"]), "Item isn't a sequential array");
    }

    /**
     * checkIfAssoc
     *
     * Checks if item is an associative array
     *
     * @param $item
     */
    private function checkIfAssoc($item)
    {
        $this->assertInternalType("array", $item, "Item isn't an array");
        $this->assertEquals(false, isset($item[0]), "Item isn't an associative array");
        $this->assertEquals(true, isset($item["key"]), "Item isn't an associative array");
    }
}
