<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 * @license MIT
 *
 * Date: 06.09.17
 * Time: 17:51
 *
 * KurzInfo zur Datei
 */

namespace Fahrenholz\PdoDatabaseBundle\Tests\Generic\Connections;

/**
 * Interface ConnectionTestInterface
 *
 * Interface for connection-testing
 *
 * @category  Interface
 * @package   Fahrenholz\PdoDatabaseBundle\Tests\Generic\Connections
 * @author    Vincent Fahrenholz <fahrenholz@strato.de>
 * @copyright 2018 Strato AG
 * @version   Release: 1.0.0
 */
interface ConnectionTestInterface
{
    /**
     * testConnectionCreationFailure
     *
     * Testet das Erstellen einer DblibConnection-Instanz ohne gültige Konfiguration
     */
    public function testConnectionCreationFailure();

    /**
     * testConnectionCreation
     *
     * Tests the creation of a connection
     */
    public function testConnectionCreation();

    /**
     * testPreparedStatementExecutionWithSyntaxError
     *
     * Tests the execution of an SQL-Query with syntax-errors with prepared statements
     */
    public function testPreparedStatementExecutionWithSyntaxError();

    /**
     * testPreparedStatementExecutionOnMissingTable
     *
     * Tests the execution of an SQL-Query with prepared statements on a missing table
     */
    public function testPreparedStatementExecutionOnMissingTable();

    /**
     * testPreparedStatementExecutionWithoutParameters
     *
     * Tests the execution of an SQL-Query with prepared statements, but without parameters
     */
    public function testPreparedStatementExecutionWithoutParameters();

    /**
     * testPreparedStatementExecutionWithParameters
     *
     * Tests the execution of an SQL-Query with prepared statements and parameters
     */
    public function testPreparedStatementExecutionWithParameters();

    /**
     * testPreparedStatementExecutionWithoutResults
     *
     * Tests the execution of an SQL-Query with prepared statements returning no result
     */
    public function testPreparedStatementExecutionWithoutResults();

    /**
     * testSimpleSqlExecutionWithoutParameters
     *
     * Tests the execution of an SQL-Query without prepared statements or parameters
     */
    public function testSimpleSqlExecutionWithoutParameters();

    /**
     * testSimpleSqlExecutionWithParameters
     *
     * Tests the execution of an SQL-Query without prepared statements and with parameters
     */
    public function testSimpleSqlExecutionWithParameters();

    /**
     * testGetSqlWithoutParameters
     *
     * Tests getSql() without Parameter-Replacement
     */
    public function testGetSqlWithoutParameters();

    /**
     * testGetSqlWithParameters
     *
     * Tests getSql() with Parameter-Replacement
     */
    public function testGetSqlWithParameters();
}
