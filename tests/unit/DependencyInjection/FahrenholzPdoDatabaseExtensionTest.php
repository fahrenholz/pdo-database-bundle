<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 * @license MIT
 *
 * Date: 04.10.17
 * Time: 18:09
 *
 * KurzInfo zur Datei
 */

namespace Fahrenholz\PdoDatabaseBundle\Tests\Generic\DependencyInjection;

use Fahrenholz\PdoDatabaseBundle\DependencyInjection\FahrenholzPdoDatabaseExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Yaml;

/**
 * Class FahrenholzPdoDatabaseExtensionTest
 *
 * Test class for dependency injection
 *
 * @category  Tests
 * @package   Fahrenholz\PdoDatabaseBundle\Tests\Generic\DependencyInjection
 * @author    Vincent Fahrenholz <fahrenholz@strato.de>
 * @copyright 2017 Strato AG
 * @version   Release: 1.0.0
 */
class FahrenholzPdoDatabaseExtensionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * testServiceCreationWithDependencyInjection
     *
     * Tests the dependency injection mechanism
     */
    public function testServiceCreationWithDependencyInjection()
    {
        $extension = new FahrenholzPdoDatabaseExtension();
        $container = new ContainerBuilder();
        $configs = Yaml::parse(file_get_contents(__DIR__ . '/../../_support/configuration/databases.yml'));
        $container->registerExtension($extension);
        $extension->load($configs, $container);
        $container->compile();
        $this->assertTrue($container->hasExtension('fahrenholz_pdo_database'));
        $this->assertTrue($container->has('fahrenholz_pdo_database.connections'));
    }
}
