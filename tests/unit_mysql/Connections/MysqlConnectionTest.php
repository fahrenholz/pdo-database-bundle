<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 * @license MIT
 *
 * Date: 06.09.17
 * Time: 17:51
 *
 * KurzInfo zur Datei
 */

namespace Fahrenholz\PdoDatabaseBundle\Tests\Mysql\Connections;


use Fahrenholz\PdoDatabaseBundle\Connections\AbstractConnection;
use Fahrenholz\PdoDatabaseBundle\Connections\ConnectionInterface;
use Fahrenholz\PdoDatabaseBundle\Connections\GenericConnection;
use Fahrenholz\PdoDatabaseBundle\Exceptions\InvalidParameterException;
use Fahrenholz\PdoDatabaseBundle\Exceptions\NoResultException;
use Fahrenholz\PdoDatabaseBundle\Result\Collection;
use Fahrenholz\PdoDatabaseBundle\Tests\Generic\Connections\ConnectionTestInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class MysqlConnectionTest
 *
 * Tests for the Mysql-Connection
 *
 * @category  Test
 * @package   Fahrenholz\PdoDatabaseBundle\Tests\Mysql\Connections
 * @author    Vincent Fahrenholz <fahrenholz@numoon.net>
 * @copyright 2017 Vincent Fahrenholz
 * @version   Release: 1.0.0
 */
class MysqlConnectionTest extends \Codeception\Test\Unit implements ConnectionTestInterface
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * _before
     *
     * Wird vor jedem Test ausgeführt und holt die Konfiguration
     */
    protected function _before()
    {
        $globalConfig = Yaml::parse(file_get_contents(__DIR__ . "/../../_support/configuration/databases.yml"));
        $this->configuration = $globalConfig["fahrenholz_pdo_database"]["mysql"];
    }

    /**
     * @inheritdoc
     */
    public function testConnectionCreationFailure()
    {
        $this->expectException(InvalidParameterException::class);
        new GenericConnection([]);
    }

    /**
     * @inheritdoc
     */
    public function testConnectionCreation()
    {
        $connection = new GenericConnection($this->configuration);
        $this->assertInstanceOf(GenericConnection::class, $connection, "Ist keine Instanz von MysqlConnection");
        $this->assertInstanceOf(ConnectionInterface::class, $connection, "Ist keine Instanz von ConnectionInterface");
    }

    /**
     * @inheritdoc
     */
    public function testPreparedStatementExecutionWithSyntaxError()
    {
        $this->expectException(\PDOException::class);
        $sql = "SELECT count(*) AS rowsCount FROOM test_data";
        $connection = new GenericConnection($this->configuration);
        $connection->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function testPreparedStatementExecutionOnMissingTable()
    {
        $this->expectException(\PDOException::class);
        $sql = "SELECT count(*) AS rowsCount FROM test_data_missing";
        $connection = new GenericConnection($this->configuration);
        $connection->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function testPreparedStatementExecutionWithoutParameters()
    {
        $sql = "SELECT count(*) AS rowsCount FROM test_data";
        $connection = new GenericConnection($this->configuration);
        $result = $connection->execute($sql);
        $this->assertInstanceOf(Collection::class, $result, "Resultat ist keine Collection");
        $item = $result->get();
        $this->assertObjectHasAttribute("rowsCount", $item, "Resultat hat die falschen Columns");
        $this->assertEquals(true, is_numeric($item->rowsCount), "Resultat scheint falsch zu sein");
    }

    /**
     * @inheritdoc
     */
    public function testPreparedStatementExecutionWithParameters()
    {
        $sql = "SELECT count(*) AS rowsCount FROM test_data WHERE id > :minId";
        $connection = new GenericConnection($this->configuration);
        $result = $connection->execute($sql, [
            ":minId" => 2,
        ]);
        $this->assertInstanceOf(Collection::class, $result, "Resultat ist keine Collection");
        $item = $result->get();
        $this->assertObjectHasAttribute("rowsCount", $item, "Resultat hat die falschen Columns");
        $this->assertEquals(18, $item->rowsCount, "Resultat scheint falsch zu sein");
    }

    /**
     * @inheritdoc
     */
    public function testPreparedStatementExecutionWithoutResults()
    {
        $this->expectException(NoResultException::class);
        $sql = "SELECT * FROM test_data WHERE id > :minId";
        $connection = new GenericConnection($this->configuration);
        $connection->execute($sql, [
            ":minId" => 1000000000,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function testSimpleSqlExecutionWithoutParameters()
    {
        $sql = "SELECT count(*) AS rowsCount FROM test_data";
        $connection = new GenericConnection($this->configuration);
        $result = $connection->executeSimple($sql);
        $this->assertInstanceOf(Collection::class, $result, "Resultat ist keine Collection");
        $item = $result->get();
        $this->assertObjectHasAttribute("rowsCount", $item, "Resultat hat die falschen Columns");
        $this->assertEquals(20, $item->rowsCount, "Resultat scheint falsch zu sein");
    }

    /**
     * @inheritdoc
     */
    public function testSimpleSqlExecutionWithParameters()
    {
        $sql = "SELECT count(*) AS rowsCount FROM test_data WHERE id > :minId";
        $connection = new GenericConnection($this->configuration);
        $result = $connection->executeSimple($sql, [
            ":minId" => 2,
        ]);
        $this->assertInstanceOf(Collection::class, $result, "Resultat ist keine Collection");
        $item = $result->get();
        $this->assertObjectHasAttribute("rowsCount", $item, "Resultat hat die falschen Columns");
        $this->assertEquals(18, $item->rowsCount, "Resultat scheint falsch zu sein");
    }

    /**
     * @inheritdoc
     */
    public function testGetSqlWithoutParameters()
    {
        $sql = "SELECT count(*) AS rowsCount FROM test_data";
        $connection = new GenericConnection($this->configuration);
        $this->assertEquals($sql, $connection->getSql($sql), "SQL stimmt nicht mit Erwartung überein");
    }

    /**
     * @inheritdoc
     */
    public function testGetSqlWithParameters()
    {
        $sql = "SELECT count(*) AS rowsCount FROM test_data WHERE id > :minId";
        $expectedSql = "SELECT count(*) AS rowsCount FROM test_data WHERE id > 2";
        $connection = new GenericConnection($this->configuration);
        $this->assertEquals($expectedSql, $connection->getSql($sql, [
            ":minId" => 2
        ]), "SQL stimmt nicht mit Erwartung überein");
    }

    /**
     * @inheritdoc
     */
    public function testQueryLog()
    {
        $sql = "SELECT count(*) AS rowsCount FROM test_data";
        $connection = new GenericConnection($this->configuration);
        $connection->execute($sql);
        $queryLog = $connection->getQueryLog();

        $keys = array_keys($queryLog);

        $this->assertCount(1, $queryLog, "getQueryLog tut nicht wie es sollte");
        $this->assertEquals($connection->getSql($sql), $queryLog[$keys[0]]->sql, "getQueryLog tut nicht wie es sollte");
        $this->assertNotEquals(null, $queryLog[$keys[0]]->executionTime, "getQueryLog tut nicht wie es sollte");
    }
}
