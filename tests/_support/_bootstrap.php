<?php
/**
 * This file is part of FahrenholzPdoDatabaseBundle
 *
 * Created by Vincent Fahrenholz <fahrenholz@numoon.net>
 * @license MIT
 *
 * Date: 06.09.17
 * Time: 14:25
 *
 * KurzInfo zur Datei
 */

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Yaml\Yaml;

$config = Yaml::parse(file_get_contents(__DIR__ . "/configuration/databases.yml"));
$container = new Container();
$container->setParameter("fahrenholz_pdo_database.config", $config["fahrenholz_pdo_database"]);

return $container;
