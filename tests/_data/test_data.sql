-- test data created with mockaroo

drop table if exists test_data;

create table if not exists test_data (
   id INT,
   first_name VARCHAR(50),
   last_name VARCHAR(50),
   email VARCHAR(50),
   gender VARCHAR(50),
   ip_address VARCHAR(20)
);

insert into test_data (id, first_name, last_name, email, gender, ip_address) values (1, 'Lenette', 'Geeson', 'lgeeson0@unicef.org', 'Female', '106.250.96.143');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (2, 'Joell', 'Fessier', 'jfessier1@edublogs.org', 'Female', '240.156.60.220');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (3, 'Anica', 'Etherington', 'aetherington2@berkeley.edu', 'Female', '141.2.179.133');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (4, 'Milka', 'Gretton', 'mgretton3@salon.com', 'Female', '181.241.31.98');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (5, 'Eolanda', 'Simmons', 'esimmons4@auda.org.au', 'Female', '202.146.144.53');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (6, 'Robby', 'Pigdon', 'rpigdon5@plala.or.jp', 'Male', '245.208.146.2');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (7, 'Winnie', 'Godthaab', 'wgodthaab6@netvibes.com', 'Male', '162.85.163.95');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (8, 'Emery', 'Presman', 'epresman7@multiply.com', 'Male', '206.150.51.118');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (9, 'Christina', 'Varcoe', 'cvarcoe8@illinois.edu', 'Female', '25.79.132.187');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (10, 'Davie', 'Bowdon', 'dbowdon9@marketwatch.com', 'Male', '85.214.194.252');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (11, 'Andriana', 'Lippiello', 'alippielloa@forbes.com', 'Female', '86.125.114.5');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (12, 'Corrine', 'Testo', 'ctestob@acquirethisname.com', 'Female', '28.127.245.60');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (13, 'Zachariah', 'de Broke', 'zdebrokec@cbsnews.com', 'Male', '165.32.128.138');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (14, 'Lock', 'Zohrer', 'lzohrerd@tiny.cc', 'Male', '174.219.34.98');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (15, 'Nil', 'Borland', 'nborlande@usgs.gov', 'Male', '101.14.236.179');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (16, 'Aurea', 'Beeho', 'abeehof@phpbb.com', 'Female', '9.177.78.162');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (17, 'Alie', 'Bend', 'abendg@w3.org', 'Female', '160.175.156.175');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (18, 'Flossy', 'Norridge', 'fnorridgeh@tripod.com', 'Female', '83.136.91.253');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (19, 'Dallon', 'Brownsett', 'dbrownsetti@lycos.com', 'Male', '152.174.16.107');
insert into test_data (id, first_name, last_name, email, gender, ip_address) values (20, 'Arther', 'Clinch', 'aclinchj@w3.org', 'Male', '77.66.36.225');
